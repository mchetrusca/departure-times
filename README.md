# Departure Times #

## What is it? ##

This is a **prototype** for a project "Departure Times", as described by [this challenge](https://github.com/uber/coding-challenge-tools/blob/master/coding_challenge.md#departure-times). This is an app which geo-localizes the user then gives the next departures from the closest public transport station.

## Technical specs ##

As specified [here](https://github.com/uber/coding-challenge-tools/blob/master/coding_challenge.md#technical-spec) I selected the **Back-end track**. Here is the list of technologies used:

* Python & Django - I chose them because I think these are the right tools for this problem, given the amount of time I had;
* [511 Real-Time Transport Service](http://assets.511.org/pdf/RTT%20API%20V2.0%20Reference.pdf) and [Google Places API](https://developers.google.com/places/web-service/search#TextSearchRequests) - I access these services to find the closest station to the user and the schedule/data about public transport;
* [AWS Elastic Beanstalk](https://aws.amazon.com/documentation/elastic-beanstalk/) - I host & run the app here;
* jQuery and HTML5 - in my minimal frontend there is just an ajax call and a geo-location call.

**All these technologies are new to me.** I played a bit with Python and Django a few years ago at the university during the labs. I selected all these technologies because I consider they are good for what I wanted to do. **I don't think my personal experience/knowledge should affect the technologies I select.** I had a lot of fun learning them while I was doing this project.

## API ##

**The important thing here is the service behind the app.** Example of usage:
```
https://<domain>/departureTimesApp/departures?latitude=<float>&longitude=<float>
```
where:

* <domain> is departure-times.kb28zrcjpn.us-west-1.elasticbeanstalk.com;
* (latitude, longitude) are the coordinates of the user.

As a response, you get the following JSON:
```
{
  'stop' : <stop-name>,
  'departures' : [
                   {
                     'route' : <string, route-name>,
                     'direction' : <string, direction, or empty string if this route does not have directions>,
                     'time' : <int, number of minutes till next transport>
                   },
                   ...
                 ],
  'error' : <present only if something went wrong>
}
```
The departures array is sorted in chronological order. Example:

https://www.departure-times.kb28zrcjpn.us-west-1.elasticbeanstalk.com/departureTimesApp/departures?latitude=37.761189&longitude=-122.419564

## How it works? ##

**THE** code is contained in [departuretimes](https://bitbucket.org/mchetrusca/departure-times/src/84ed15dbb605afad6d744145fcb95fe3b44c217c/DepartureTimesProject/departuretimes/?at=master) package, which is independent and unaware of Django. Once we have a location of the user there are 2 problems:

1. Map user's location to a bus station - let's call it **StationFinder**;
2. Get predictions/schedule for that station - **DepartureProvider**.

Both of these problems can be solved by different tools and the implementation of one should not be tied to the second. These two interact through **interfaces**. For example, in this concrete implementation, the only provider of public transport data is the above-mentioned 511 service. If tomorrow we decide to add another provider, let's say [nextBus](http://www.nextbus.com/xmlFeedDocs/NextBusXMLFeed.pdf) (or even a provider for a different city/region), we extend the interface and add a few lines of instantiation in our [DepartureTimesFactory](https://bitbucket.org/mchetrusca/departure-times/src/84ed15dbb605afad6d744145fcb95fe3b44c217c/DepartureTimesProject/departuretimes/departure_times_factory.py?at=master&fileviewer=file-view-default).

Since retrieving all data every time we try to find departures from 511 is unfeasible (there are about 14k of stops), I [stored locally](https://bitbucket.org/mchetrusca/departure-times/src/84ed15dbb605afad6d744145fcb95fe3b44c217c/DepartureTimesProject/departuretimes/provider_511/stations_persistence.py?at=master&fileviewer=file-view-default) the data which doesn't change often. Please see the TODO list below regarding what thing can be improved in the current solution.

On the other hand, for station finding I use Google Places API. I search for nearby transport stations, then select the closest one. An interesting problem popped out during the implementation - station names identified by Google **are different** from the ones stored at 511. See [this funny solution/mapping](https://bitbucket.org/mchetrusca/departure-times/src/84ed15dbb605afad6d744145fcb95fe3b44c217c/DepartureTimesProject/departuretimes/provider_511/google_station_finder.py?at=master&fileviewer=file-view-default#google_station_finder.py-66) which I found.

## TODO: ##

1. Complete periodical stations sync from remote;
2. Mock remote/dependency functionalities in order to cover all code by unit tests;
3. Change the storage of data;
4. More testing for other browsers;
5. Improve front end, UI and UX;
6. Improve localization of the user;
7. Optimizations: cache station locations from google; "Accept-Encoding: gzip, deflate";
8. Change the storage option to use a real db;
9. Fix the self-signed certificate issue (use one from AWS?).

## Questions? ##

Please reach me if you have any questions. I would love to hear your feedback regarding this approach I've chosen, especially that I kind of experimented with technologies. Thanks!