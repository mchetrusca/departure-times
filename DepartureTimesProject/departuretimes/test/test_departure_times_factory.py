import unittest
from departuretimes.departure_times_factory import *
from departuretimes.provider_511.departure_provider_511 import DepartureProvider511
from departuretimes.provider_511.google_station_finder import GoogleStationFinder


class TestDepartureTimesFactory(unittest.TestCase):

    def test_build_departure_provider(self):
        stationFinder = build_station_finder(GOOGLE)
        provider511 = DepartureProvider511(stationFinder)
        provider = build_departure_provider(PROVIDER_511, stationFinder)
        self.assertEqual(type(provider), type(provider511))

    def test_build_station_provider(self):
        stationFinder = build_station_finder(GOOGLE) 
        googleStationFinder = GoogleStationFinder()
        self.assertEqual(type(stationFinder), type(googleStationFinder))      

if __name__ == '__main__':
    unittest.main()