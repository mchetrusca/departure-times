""" Uses http://511.org/developer-resources_transit-api.asp service to
    get departure predictions given a place and a station finder.
"""
from operator import itemgetter
from multiprocessing.dummy import Pool as ThreadPool
from departuretimes.departure_provider import DepartureProvider
import provider_511_client
import stations_persistence

class DepartureProvider511(DepartureProvider):
    """ Concrete implementation for DepartureProvider, using 511 service.
        Returns a list of departures sorted in chronological order.
    """

    def __init__(self, stationFinder):
        """ Basic init.

            Args:
                stationFinder (StationFinder):  someone who maps coordinates to stations.
        """
        super(DepartureProvider511, self).__init__()
        self.stationFinder = stationFinder

    def provide_departures(self, location):
        """ Given a location, fetches departure predictions for the nearby station.

            Args:
                location (tuple): (float, float) = (latitude, longitude)

            Returns:
                a list of departures represented as dictionaries:
                {
                'stop' : <stop_name>,
                 'route' : <route_name>,
                 'time' : <time>
                }
                <time> is int and the others are str.
                The list is sorted in ascending order by time.
        """
        stopName = self.stationFinder.find_station(location)
        if not stopName:
            return []
        stopsDict = self.stationFinder.allStopsDict
        stopCodes = stopsDict[stopName]
        departureTimes = []
        pool = ThreadPool(len(stopCodes))
        results = pool.map(provider_511_client.get_departures_list_for_stopCode, stopCodes)
        departureTimes = results[0]
        return sorted(departureTimes, key=itemgetter('time'))

