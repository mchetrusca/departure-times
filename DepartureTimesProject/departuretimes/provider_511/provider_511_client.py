""" A handler to talk with 511 service.
    Refer to http://assets.511.org/pdf/RTT%20API%20V2.0%20Reference.pdf
    for documentation of 511 Service.
"""

import pickle
import urllib2
import xml.etree.ElementTree
from urllib import quote
from datetime import datetime

PROVIDER_URL = 'http://services.my511.org/Transit2.0/'
GET_AGENCIES = 'GetAgencies.aspx'
GET_ROUTES = 'GetRoutesForAgency.aspx'
GET_STOPS = 'GetStopsForRoute.aspx'
GET_DEPARTURES = 'GetNextDeparturesByStopCode.aspx'
APP_TOKEN = '?token=695cc3e1-ea7b-4ab1-a12d-f75a182a5f04'

AGENCY_NAME = '&agencyName='
ROUTE_IDF = '&routeIDF='
STOP_CODE_URL_ARG = "&stopcode="

ERROR1 = 'TransitServiceError'
ERROR2 = 'transitServiceError'
AGENCY_LIST = 'AgencyList'
AGENCY = 'Agency'
NAME = 'Name'
HAS_DIRECTION = 'HasDirection'
ROUTE_LIST = 'RouteList'
ROUTE = 'Route'
CODE = 'Code'
ROUTE_DIRECTION_LIST = 'RouteDirectionList'
ROUTE_DIRECTION = 'RouteDirection'
STOP_LIST = 'StopList'
STOP = 'Stop'
STOP_CODE = 'StopCode'
STOP_NAME = 'name'
DEPARTURE_TIME_LIST = 'DepartureTimeList'
DEPARTURE_TIME = 'DepartureTime'

def get_all_stops():
    """ Retrieves all the stops from the 511.

        Returns:
            a list of tuples (StopName, StopCode) as defined by 511 provider.
    """
    agencyList = get_all_agencies()
    stopList = []
    for agency in agencyList:
        routeList = get_routes_for_agency(agency[0])
        for route in routeList:
            routeIDFs = []
            routeIDF = quote(agency[0]) + '~' + quote(route[0])
            # if this agency's routes have directions:
            if agency[1] == 'True':
                for direction in route[1]:
                    routeIDFs.append(routeIDF + '~' + quote(direction))
            else:
                routeIDFs.append(routeIDF)
            for routeIDF in routeIDFs:
                stopList += get_stop_list_for_routeIDF(routeIDF)
    return stopList

def get_all_agencies():
    """ Retrieves all the agencies from 511.

        Returns:
            a list of tuples (AGENCY_NAME, bool hasDirection).
    """
    agenciesURL = PROVIDER_URL + GET_AGENCIES + APP_TOKEN
    agenciesXMLString = get(agenciesURL)
    agencyList = []
    root = xml.etree.ElementTree.fromstring(agenciesXMLString)
    if root.tag == ERROR1  or root.tag == ERROR2:
        return []
    agencyListElement = root.find(AGENCY_LIST)
    for agencyElement in agencyListElement.findall(AGENCY):
        agencyList.append((agencyElement.get(NAME), agencyElement.get(HAS_DIRECTION)))
    return agencyList

def get_routes_for_agency(agency):
    """ Routes are codes + directions.

        Args:
            agency (str): name of the agency.

        Returns:
            a list of tuples (CODE, listOfDirections).
    """
    routesURL = PROVIDER_URL + GET_ROUTES + APP_TOKEN + AGENCY_NAME + quote(agency)
    routesXMLString = get(routesURL)
    root = xml.etree.ElementTree.fromstring(routesXMLString)
    if root.tag == ERROR1  or root.tag == ERROR2:
        return []
    hasDirection = root.find(AGENCY_LIST).find(AGENCY).get(HAS_DIRECTION)
    routeListElement = root.find(AGENCY_LIST).find(AGENCY).find(ROUTE_LIST)
    routeList = []
    for routeElement in routeListElement.findall(ROUTE):
        directions = []
        if hasDirection == 'True':
            directionElements = routeElement.find(ROUTE_DIRECTION_LIST).findall(ROUTE_DIRECTION)
            for directionElement in directionElements:
                directions.append(directionElement.get(CODE))
        routeList.append((routeElement.get(CODE), directions))
    return routeList

def get_stop_list_for_routeIDF(routeIDF):
    """ Gets stops for a route.

        Args:
            routeIDF (str): composed of <agency_name>~<route_name>~<direction>
                ~<direction> should be present only if this route hasDirection.
                Each above identifier should be URL escaped.

        Returns:
        a list of tuples (STOP_NAMEs, STOP_CODEs)
    """
    stopURL = PROVIDER_URL + GET_STOPS + APP_TOKEN + ROUTE_IDF + routeIDF
    stopsXMLString = get(stopURL)
    root = xml.etree.ElementTree.fromstring(stopsXMLString)
    if root.tag == ERROR1  or root.tag == ERROR2:
        return []
    # if we have 2 '~' it means we specified direction as well:
    if routeIDF.count('~') == 2:
        stopListElement = root.find(AGENCY_LIST).find(AGENCY).find(ROUTE_LIST).find(ROUTE).find(ROUTE_DIRECTION_LIST).find(ROUTE_DIRECTION).find(STOP_LIST)
    else:
        stopListElement = root.find(AGENCY_LIST).find(AGENCY).find(ROUTE_LIST).find(ROUTE).find(STOP_LIST)
    stopList = []
    for stopElement in stopListElement.findall(STOP):
        stopList.append((stopElement.get(STOP_NAME), stopElement.get(STOP_CODE)))
    return stopList

def get(URL):
    """ Utility function.

        Args:
            URL (str): the URL of the request.

        Returns:
            the response as a string of the requested URL.
    """
    xmlFile = urllib2.urlopen(URL)
    return xmlFile.read()

def get_departures_list_for_stopCode(stopCode):
    """ Retrieves predictions from 511 regarding the departures from a station.

        Args:
            stopCode (str): id for stop as defined by 511.

        Returns:
            a list of ditionaries
            {
             'route' : <route_name>,
             'direction' : <direction or ''>,
             'time' : <time> (int)
            }
            of departures for a given stop code.
            <time> is int and the others are str.
    """
    departureURL = PROVIDER_URL + GET_DEPARTURES + APP_TOKEN + STOP_CODE_URL_ARG + stopCode
    departureXMLString = get(departureURL)
    root = xml.etree.ElementTree.fromstring(departureXMLString)
    if root.tag == ERROR1  or root.tag == ERROR2:
        return []
    hasDirection = root.find(AGENCY_LIST).find(AGENCY).get(HAS_DIRECTION)
    routeListElement = root.find(AGENCY_LIST).find(AGENCY).find(ROUTE_LIST)
    departureList = []
    for routeElement in routeListElement.findall(ROUTE):
        direction = ''
        stopName = None
        if hasDirection:
            departureTimeListElement = routeElement.find(ROUTE_DIRECTION_LIST).find(ROUTE_DIRECTION).find(STOP_LIST).find(STOP).find(DEPARTURE_TIME_LIST)
            direction = routeElement.find(ROUTE_DIRECTION_LIST).find(ROUTE_DIRECTION).get(CODE)
            stopName = routeElement.find(ROUTE_DIRECTION_LIST).find(ROUTE_DIRECTION).find(STOP_LIST).find(STOP).get(STOP_NAME)
        else:
            departureTimeListElement = routeElement.find(STOP_LIST).find(STOP).find(DEPARTURE_TIME_LIST)
            stopName = routeElement.find(STOP_LIST).find(STOP).get(STOP_NAME)
        for departureTimeElement in departureTimeListElement.findall(DEPARTURE_TIME):
            departureList.append({ 'route' : str(routeElement.get(NAME)), 'direction' : str(direction), 'time' : int(departureTimeElement.text)})
    return departureList