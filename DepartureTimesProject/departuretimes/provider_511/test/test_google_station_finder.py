import unittest
from departuretimes.provider_511.google_station_finder import GoogleStationFinder

class TestGoogleStationFinder(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestGoogleStationFinder, self).__init__(*args, **kwargs)
        self.googleFinder = GoogleStationFinder()
        self.location = ('37.7618891', '-122.4161173')    

    def test_find_station(self):
        stop511 = self.googleFinder.find_station(self.location)
        self.assertEquals(stop511, 'Folsom St and 18th St')

    def test_get_closest_stop(self):
        googleStop = self.googleFinder.get_closest_stop(self.location)
        self.assertEquals(googleStop, 'Folsom St & 18th St')

    def test_find_511_station(self):
        wantedStop = 'Folsom St and 18th St'
        allStopsList = ['Broadway and 4th St',
                        'KITTYHAWK AT FORD DEALER',
                        wantedStop,
                        'Folsom and Cesar Chavz St',
                        '18th St and Mission St']
        googleStop = 'Folsom St & 18th St'
        stop511 = self.googleFinder.find_511_station(googleStop, allStopsList)
        self.assertEquals(stop511, wantedStop)      

if __name__ == '__main__':
    unittest.main()