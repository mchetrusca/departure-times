import unittest
from departuretimes.provider_511.stations_persistence import *

class TestStationsPersistence(unittest.TestCase):
    """ TODO: Mock remote functionality and test things.
    """

    def test_get_stops_dictionary(self):
        stops = get_stops_dictionary()
        key = stops.keys()[0]
        self.assertTrue(isinstance(key, str))
        value = stops[key]
        self.assertTrue(isinstance(value, list))
        
    def test_sync_is_needed(self):
        result = sync_is_needed()
        self.assertTrue(isinstance(result, bool))

    def test_update_last_sync(self):
        update_last_sync()

if __name__ == '__main__':
    unittest.main()