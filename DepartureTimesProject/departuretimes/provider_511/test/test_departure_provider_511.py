import unittest
from departuretimes.provider_511.departure_provider_511 import DepartureProvider511
from departuretimes.provider_511.google_station_finder import GoogleStationFinder

class TestDepartureProvider511(unittest.TestCase):
    """ TODO: Mock remote functionality and test things.
    """

    def test_provide_departures(self):
        stationFinder = GoogleStationFinder()
        provider = DepartureProvider511(stationFinder)
        location = ('37.7618891', '-122.4161173')
        departures = provider.provide_departures(location)

if __name__ == '__main__':
    unittest.main()