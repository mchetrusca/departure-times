""" This module defines how to find closest station from those provided by 511
    using Google Maps API.
    For documentation, see https://developers.google.com/places/web-service/search#TextSearchRequests
"""
import xml.etree.ElementTree
import provider_511_client
import stations_persistence
from departuretimes.station_finder import StationFinder

GOOGLE_API_URL = 'https://maps.googleapis.com/maps/api/place/nearbysearch/'
OUTPUT_TYPE = 'xml'
KEY_ARG = '?key='
MY_GOOGLE_API_KEY = 'AIzaSyClDm64zlpMvaJZRV1c7EhPDqBRd-lLFoc'
LOCATION_ARG = '&location='
RANKBY = '&rankby=distance'
TYPE = '&type=transit_station'
RESULT = 'result'
NAME = 'name'

class GoogleStationFinder(StationFinder):
    """ Given a location, maps it to a station from 511 db using Google Maps API.
    """

    def __init__(self):
        self.allStopsDict = stations_persistence.get_stops_dictionary()
        self.currentStation = ''

    def find_station(self, location):
        """ Args:
                location (tuple): (float, float) = (latitude, longitude)

            Returns:
                str, name of the station, '' if unsuccessfull.

            Example:
                station = stationFinder.find_station((37.807, -122.47))
        """
        googleStop = self.get_closest_stop(location)
        allStopsNamesList = self.allStopsDict.keys()
        self.currentStation = self.find_511_station(googleStop, allStopsNamesList)
        return self.currentStation

    def get_closest_stop(self, userLocation):
        """ Returns the station name as it is on Google Maps.

            Args:
                userLocation (tuple): (float, float) = (latitude, longitude)

            Returns:
                str, name of the station, '' if unsuccessfull.

            Example:
                googleStation = stationFinder.find_station((37.807, -122.47))
        """
        location = LOCATION_ARG + str(userLocation[0]) + ',' + str(userLocation[1])
        locationSearchURL = GOOGLE_API_URL + OUTPUT_TYPE + KEY_ARG + MY_GOOGLE_API_KEY + location + RANKBY + TYPE
        xmlString = provider_511_client.get(locationSearchURL)
        root = xml.etree.ElementTree.fromstring(xmlString)
        results = root.find(RESULT)
        if not results:
            googleFetchedName = ''
            return ''
        googleFetchedName = results.find(NAME).text
        return googleFetchedName

    def find_511_station(self, googleStop, allStopsList):
        """ Associates a googleStop with a stop from 511.

            Args:
                googleStop (str): station name as it appears on google
                allStopsList (list): list of station names as retrieved form 511

            Returns:
                str, station from 511 which is the same with googleStop one.

            How it works: "16th St and Bryant St" is found by google;
            Let's say from 511 we identify this stop by "Bryant & 16th St"
            As you can observe, the longer words matter the most in this comparison.
            We lowercase both strings and compute a score which is proportional to words
            which are present in the string and their length.
        """
        maxScore = 0
        station = ''
        if not googleStop:
            return ''
        googleStop = googleStop.lower()
        googleStopWords = googleStop.split()
        sorted(googleStopWords, key=len)
        for stop in allStopsList:
            stopCopy = stop
            stopCopy = stopCopy.lower()
            score = 0
            index = 1
            for googleWord in googleStopWords:
                if googleWord in stopCopy:
                    score += index * len(googleWord)
                index += 1
                if score > maxScore:
                    maxScore = score
                    station = stop
        return station