""" This module is responsible for giving access to an up-to-date version of stations
    and stop codes in a fast way. Thus it stores and updates a local version of it,
    also providing deserialization for that data. The original data is provided by
    a remote service thus accessing it every time adds too much latency.
"""

import os.path
import pickle
import datetime
import provider_511_client

LAST_SYNC_DATE_FILENAME = 'lastSync.pickle'
STATIONS_FILENAME = 'stations.pickle'

def get_stops_dictionary():
    """ Reads the contents of the STATIONS_FILENAME and returns it as a dictionary.

        Returns:
            a dictionary of type { 'stationName' : [stopCode1, stopCode2, ..., stopCodeN]}
            A stopCode uniquely identifies a (stop, route, direction).
    """
    # TODO: we should update our "data base" in the background, without blocking the main thread.
    # self.sync_if_needed()
    if os.path.isfile(STATIONS_FILENAME):
        with open(STATIONS_FILENAME, 'rb') as handle:
            return pickle.load(handle)        
    else:
        raise Exception('Could not open ' + STATIONS_FILENAME + ' file')

def sync_is_needed():
    """ Checks if a sync is needed.

        Returns:
             True if stations stored locally might be out of sync
             (that is, are older than one day), False otherwise.
    """
    if os.path.isfile(LAST_SYNC_DATE_FILENAME):
        with open(LAST_SYNC_DATE_FILENAME, 'rb') as handle:
            lastSyncDate = pickle.load(handle)
    else:
        return True
    if lastSyncDate == datetime.datetime.today().date():
        return False
    else:
        return True

def sync_if_needed():
    """ Updates the local db of stations if needed.
    """
    if sync_is_needed():
        fetch_stations_from_remote()
        update_last_sync()

def fetch_stations_from_remote():
    """ Retrieves stations from the 511 provider and stores them locally.
    """
    providerClient = Provider511Client()
    allStopsList = providerClient.get_all_stops()
    stopsDict = {}
    for stop in allStopsList:
        if stop[0] in stopsDict:
            stopsDict[stop[0]].append(stop[1])
        else:
            stopsDict[stop[0]] = [stop[1]]

    with open(STATIONS_FILENAME, 'wb') as handle:
        pickle.dump(stopsDict, handle)

def update_last_sync():
    """ Writes to a file the current date and time of sync.
    """
    with open('lastSync.pickle', 'wb') as handle:
        pickle.dump(datetime.datetime.now(), handle)
