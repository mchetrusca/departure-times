class DepartureProvider(object):
    """ Abstract class. Returns a list of upcoming departures given a location.
        See below method for details.
    """

    def provide_departures(self, location):
        """ Returns a list of departures given a location represented as dictionaries:
            {
             'stop' : <stop_name>, 
             'route' : <route_name>,
             'time' : <time>
            }
            <time> is int and the others are str.
            The list is sorted in ascending order by time.
            location is a tuple (latitude, longitude), where 
            latitude, longitude are float numbers.
        """
        raise NotImplementedError('users must define provide_departures() method to use this class')
        

