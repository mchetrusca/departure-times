""" Abstracts the creation of concrete objects in this service.
"""

from provider_511.departure_provider_511 import DepartureProvider511
from provider_511.google_station_finder import GoogleStationFinder

GOOGLE = 'Google'
PROVIDER_511 = '511 Real-Time Transit Departures'

def build_departure_provider(provider, stationFinder):
    """
        Args:
            provider (str): identifier to distinguish different departure providers.
            stationFinder (StationFinder): knows how to map a location to a station from db.

        Returns:
            a specific DepartureProvider.
    """
    if provider == PROVIDER_511:
        return DepartureProvider511(stationFinder)
    else:
        raise Exception('Unknown departure provider asked to be build.')

def build_station_finder(stationFinder):
    """
        Args:
            stationFinder (str): identifier for StationFinder-s.
    """
    if stationFinder == GOOGLE:
        return GoogleStationFinder()
    else:
        raise Exception('Unknown station finder asked to be build.')