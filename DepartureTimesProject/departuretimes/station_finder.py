class StationFinder(object):
    """ Abstract class for defining the functionality of finding the closest station to user's location.
    """

    def find_station(self, location):
        """ Returns a stop name for which a schedule can be retrieved from a provider.
        """
        raise NotImplementedError('users must define find_station() method to use this class')
