from django.http import JsonResponse
from django.shortcuts import render
from django.template import RequestContext
from departuretimes.departure_times_factory import *

def index(request):
    return render(request, 'departureTimesApp/index.html')

def departures(request):
    context = RequestContext(request)
    if request.method == 'GET':
        latitude = request.GET.get('latitude', None)
        longitude = request.GET.get('longitude', None)
        if not parametersAreValid(latitude, longitude):
            return JsonResponse({
                                'error': 'Input parameters are not valid.',
                                'stop' : '',
                                'departures' : []
                                },
                                safe=False)

    stationFinder = build_station_finder(GOOGLE)
    provider = build_departure_provider(PROVIDER_511, stationFinder)

    location = (float(latitude), float(longitude))

    nextDepartures = provider.provide_departures(location)
    response = createJsonResponse(nextDepartures, stationFinder.currentStation)
    return response

def parametersAreValid(latitude, longitude):
    try:
        float(latitude)
        float(longitude)
    except:
        return False
    else:
        return True

def createJsonResponse(departures, station):
    """ departures - a list of dictionaries.
    """
    try:
        if not station:
            return JsonResponse({
                                'error' : 'Could not determine the nearby station.',
                                'stop' : '',
                                'departures' : []
                                },
                                safe=False)
        if not departures:
            return JsonResponse({
                                'error' : 'By our sources, no transport is passing nearby for the next 90 minutes.',
                                'stop' : station,
                                'departures' : []
                                },
                                safe=False)
        else:
            response = JsonResponse({
                                    'stop' : station,
                                    'departures' : departures
                                    },
                                    safe=False)
            return response
    except:
        return JsonResponse({
                            'error' : 'Could not determine the departures.',
                            'stop' : station,
                            'departures' : []
                            },
                            safe=False)